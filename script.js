
function changeTitle(){
    var title = document.getElementById("title");
    console.log(title);
    title.innerHTML = 'SELAMAT MALAM!'
}

function submit() {
    var nama = document.getElementsByName("nama")[0];
    console.log(nama.value)

    var result = document.getElementById("result")
    result.innerHTML = `Selamat Datang, ${nama.value}`
}

console.log(document.getElementsByClassName('description'))

const description = document.getElementsByClassName('description')[0]
description.style.backgroundColor = '#b52f2f'
description.style.color = 'white'

const description2 = document.getElementsByClassName('description')[1]
description2.style.backgroundColor = 'blue'
description2.style.color = 'white'

const subtitle = document.createElement('p')
subtitle.textContent = 'Ini adalah subtitle'

document.body.append(subtitle)

const removed = document.getElementsByClassName("to-be-removed")[0]
removed.remove()

const desc = document.querySelector(".description")
desc.style.color = 'gray'

const descs = document.querySelectorAll(".description")[1]
descs.innerHTML = 'Teks ini diubah menggunakan querySelecterAll'