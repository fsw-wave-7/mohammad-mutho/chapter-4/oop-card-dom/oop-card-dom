
class Card {
    constructor(cardId) {
        this.cardId = cardId
        this._chosen = null
        this.options = [
            {
                name: 'dog',
                url: 'https://image.freepik.com/free-vector/cute-dog-sticking-her-tongue-out-cartoon-icon-illustration_138676-2709.jpg'
            },
            {
                name: 'bear',
                url: 'https://image.freepik.com/free-vector/cute-teddy-bear-waving-hand-cartoon-icon-illustration_138676-2714.jpg'
            },
            {
                name: 'cat',
                url: 'https://image.freepik.com/free-vector/cute-cat-with-cat-bowl-cartoon-icon-illustration_138676-2707.jpg'
            }
        ]
    }

    get chosen() {
        return this._chosen
    }

    set chosen(card) {
        this._chosen = card
    }

    getCard() {
        document.getElementById(this.cardId).innerHTML = ''

        const t = this
        return this.options.forEach((opt) => {

            let className = 'm-3'
            if (this._chosen != null && this._chosen.name == opt.name) {
                className = 'm-3 active'
            }

            const img = document.createElement('img')
            img.src = opt.url
            img.className = className
            img.id = `${this.cardId}-${opt.name}`

            document.getElementById(this.cardId).append(img)
        })
    }

    getOptions() {
        return this.options;
    }

    choosePic(animal) {
        document.getElementsByClassName('choose-result')[0].innerHTML = `I choose ${animal}`
    }
}

class RandomCard extends Card {
    constructor(cardId) {
        super(cardId)
    }

    random() {
        let index = Math.floor(Math.random() * Math.floor(3))
        const options = super.getOptions()
        return options[index]
    }
}

const card = new Card('cards')
card.getCard()
const cardComp = new RandomCard('cards-comp')
cardComp.getCard()

function getResult(player, comp) {
    let text = ''
    if (player.name == comp.name) {
        text = `Player and Comp choose same card : ${player.name}`
    } else {
        text = `Player and Comp choose different card. Player : ${player.name} AND Comp : ${comp.name}`
    }

    document.getElementsByClassName('result')[0].innerHTML = text
}

const options = card.getOptions()
options.forEach((opt) => {
    document.getElementById(`cards-${opt.name}`).addEventListener('click', () => {
        console.log('CeK')
        card.chosen = opt
        cardComp.chosen = cardComp.random()
        getResult(card.chosen, cardComp.chosen) 
        card.getCard()
        cardComp.getCard()
    })
})

document.getElementById('reset').addEventListener('click', () => {
    card.chosen = null
    cardComp.chosen = null
    card.getCard()
    cardComp.getCard()
})



